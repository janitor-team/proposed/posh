# These tests deal with tokens.
#
name: token-1
description:
	Check expansion in a quoted heredoc
category: debian,posix
stdin:
	FULLVARIABLE="Three short words"
	cat <<"DELIMITER"
	One $EMPTYVARIABLE
	Two $FULLVARIABLE
	Three $(echo command expansion)
	Four `echo backquote expansion`
	Five $(( 2 + 3 ))
	DELIMITER
expected-stdout:
	One $EMPTYVARIABLE
	Two $FULLVARIABLE
	Three $(echo command expansion)
	Four `echo backquote expansion`
	Five $(( 2 + 3 ))
---
name: token-2
description:
	Check expansion in an unquoted heredoc
category: debian,posix
stdin:
	FULLVARIABLE="Three short words"
	cat <<DELIMITER
	One $EMPTYVARIABLE
	Two $FULLVARIABLE
	Three $(echo command expansion)
	Four `echo backquote expansion`
	Five $(( 2 + 3 ))
	DELIMITER
expected-stdout:
	One 
	Two Three short words
	Three command expansion
	Four backquote expansion
	Five 5
---
name: token-3
description:
	Check expansion in a hyphenated heredoc
category: debian,posix
stdin:
	FULLVARIABLE="Three short words"
	cat <<-DELIMITER
		One $EMPTYVARIABLE
			Two $FULLVARIABLE
				Three $(echo command expansion)
					Four `echo backquote expansion`
						Five $(( 2 + 3 ))
	DELIMITER
expected-stdout:
	One 
	Two Three short words
	Three command expansion
	Four backquote expansion
	Five 5
---
name: token-4
description:
	Check expansion in multiple heredocs
category: debian,posix
stdin:
	FULLVARIABLE="Three short words"
	echo FIRST ; cat <<DELIMITER1 ; echo SECOND ; cat <<DELIMITER2 ; echo DONE
	One $EMPTYVARIABLE
	Two $FULLVARIABLE
	Three $(echo command expansion)
	Four `echo backquote expansion`
	Five $(( 2 + 3 ))
	DELIMITER1
	Six $EMPTYVARIABLE
	Seven $FULLVARIABLE
	Eight $(echo command expansion)
	Nine `echo backquote expansion`
	Ten $(( 2 + 3 ))
	DELIMITER2
expected-stdout:
	FIRST
	One 
	Two Three short words
	Three command expansion
	Four backquote expansion
	Five 5
	SECOND
	Six 
	Seven Three short words
	Eight command expansion
	Nine backquote expansion
	Ten 5
	DONE
---
name: token-5
description:
	Check file redirection
category: debian,posix
stdin:
	TEMPFILE=$(tempfile --prefix posix)
	echo Sesame >$TEMPFILE
	exec 8<$TEMPFILE
	read LINE <&8
	echo $LINE seeds
	rm $TEMPFILE
expected-stdout:
	Sesame seeds
---
name: token-6
description:
	Check fd redirection
category: debian,posix
stdin:
	exec 8>&1
	echo Hush ; echo Puppies >&8 | cat - 8>&1
expected-stdout:
	Hush
	Puppies
---
name: token-7
description:
	Check random access
category: debian,posix
stdin:
	TEMPFILE=$(tempfile --prefix posix)
	echo Sesame oil >$TEMPFILE
	echo Snake oil >>$TEMPFILE
	echo Baby oil >>$TEMPFILE
	
	cat $TEMPFILE
	echo Followed by revision
	
	exec 8<>$TEMPFILE
	read LINE <&8
	echo Tahini >&8
	read LINE <&8
	exec 8>&-
	
	cat $TEMPFILE
	
	rm $TEMPFILE
expected-stdout:
	Sesame oil
	Snake oil
	Baby oil
	Followed by revision
	Sesame oil
	Tahini
	il
	Baby oil
---
name: token-8
description:
	Check underscores
category: debian,posix
stdin:
	BLAH_THIS=aglio
	echo $BLAH_THIS
	BLAH_THIS=olio
	echo $BLAH_THIS
expected-stdout:
	aglio
	olio
---
